import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
const path = require("path");
// https://vitejs.dev/config/
export default defineConfig({
  test: {
    globals: true,
    watch: true,
    environment: 'jsdom',
  },
  resolve: {
    extensions: [
      ".mjs",
      ".js",
      ".ts",
      ".jsx",
      ".tsx",
      ".json",
      ".vue",
      ".scss",
    ],
    alias: {
      '@': path.resolve(__dirname, './src')
    },
  },
  plugins: [vue()]
})


// import { defineConfig } from 'vite'

// export default defineConfig({
//   test: {
//     environment: 'jsdom',
//   },
// })