"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEventId = createEventId;
exports.INITIAL_EVENTS = void 0;
var eventGuid = 0;
var todayStr = new Date().toISOString().replace(/T.*$/, ''); // YYYY-MM-DD of today

var INITIAL_EVENTS = [{
  id: createEventId(),
  title: 'All-day event',
  start: todayStr
}, {
  id: createEventId(),
  title: 'Timed event',
  start: todayStr + 'T12:00:00'
}];
exports.INITIAL_EVENTS = INITIAL_EVENTS;

function createEventId() {
  return String(eventGuid++);
}