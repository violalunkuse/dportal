"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vueRouter = require("vue-router");

var _Main = _interopRequireDefault(require("../layouts/side-menu/Main.vue"));

var _Main2 = _interopRequireDefault(require("../layouts/simple-menu/Main.vue"));

var _Main3 = _interopRequireDefault(require("../layouts/top-menu/Main.vue"));

var _Main4 = _interopRequireDefault(require("../views/dashboard/Main.vue"));

var _Main5 = _interopRequireDefault(require("../views/chat/Main.vue"));

var _Main6 = _interopRequireDefault(require("../views/profile-overview/Main.vue"));

var _Main7 = _interopRequireDefault(require("../views/login/Main.vue"));

var _verify_phone = _interopRequireDefault(require("../views/login/verify_phone.vue"));

var _verify_email = _interopRequireDefault(require("../views/login/verify_email.vue"));

var _Main8 = _interopRequireDefault(require("../views/register/Main.vue"));

var _Invoice = _interopRequireDefault(require("../views/invoices/Invoice.vue"));

var _Notification = _interopRequireDefault(require("../views/notifications/Notification.vue"));

var _Settings = _interopRequireDefault(require("../views/settings/Settings.vue"));

var _Appointments = _interopRequireDefault(require("../views/appointments/Appointments.vue"));

var _Main9 = _interopRequireDefault(require("../views/error-page/Main.vue"));

var _Main10 = _interopRequireDefault(require("../views/update-profile/Main.vue"));

var _Main11 = _interopRequireDefault(require("../views/change-password/Main.vue"));

var _Main12 = _interopRequireDefault(require("../views/regular-table/Main.vue"));

var _authService = _interopRequireDefault(require("../service/auth-service"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var routes = [{
  path: '/',
  redirect: '/login'
}, {
  path: '/dashboard',
  component: _Main["default"],
  children: [{
    path: '/dashboard',
    name: 'side-menu-dashboard-overview',
    component: _Main4["default"],
    meta: {
      crumbs: [{
        name: 'Home',
        link: ''
      }]
    }
  }, {
    path: 'inquiry',
    name: 'side-menu-inquiry',
    component: _Main5["default"],
    meta: {
      crumbs: [{
        name: 'Home',
        link: '/dashboard'
      }, {
        name: 'Messages',
        link: ''
      }]
    }
  }, {
    path: 'profile',
    name: 'side-menu-profile-overview',
    component: _Main6["default"],
    meta: {
      crumbs: [{
        name: 'Home',
        link: '/dashboard'
      }, {
        name: 'Profile',
        link: ''
      }]
    }
  }, {
    path: 'invoice',
    name: 'side-menu-invoice',
    component: _Invoice["default"],
    meta: {
      crumbs: [{
        name: 'Home',
        link: '/dashboard'
      }, {
        name: 'Invoice',
        link: ''
      }]
    }
  }, {
    path: 'settings',
    name: 'side-menu-settings',
    component: _Settings["default"],
    meta: {
      crumbs: [{
        name: 'Home',
        link: '/dashboard'
      }, {
        name: 'Settings',
        link: ''
      }]
    }
  }, {
    path: 'appointments',
    name: 'side-menu-appointments',
    component: _Appointments["default"],
    meta: {
      crumbs: [{
        name: 'Home',
        link: '/dashboard'
      }, {
        name: 'Appointments',
        link: ''
      }]
    }
  }, {
    path: 'notification',
    name: 'side-menu-notification',
    component: _Notification["default"],
    meta: {
      crumbs: [{
        name: 'Home',
        link: '/dashboard'
      }, {
        name: 'Notification',
        link: ''
      }]
    }
  }, {
    path: 'update-profile',
    name: 'side-menu-update-profile',
    component: _Main10["default"],
    meta: {
      crumbs: [{
        name: 'Update Profile',
        link: ''
      }]
    }
  }, {
    path: 'change-password',
    name: 'side-menu-change-password',
    component: _Main11["default"]
  }, {
    path: 'regular-table',
    name: 'side-menu-regular-table',
    component: _Main12["default"]
  }]
}, {
  path: '/simple-menu',
  component: _Main2["default"],
  children: [{
    path: '/',
    name: 'simple-menu-dashboard-overview',
    component: _Main4["default"]
  }, {
    path: 'inquiry',
    name: 'simple-menu-inquiry',
    component: _Main5["default"]
  }, {
    path: 'profile-overview',
    name: 'simple-menu-profile-overview',
    component: _Main6["default"]
  }, {
    path: 'update-profile',
    name: 'simple-menu-update-profile',
    component: _Main10["default"]
  }, {
    path: 'invoice',
    name: 'invoice',
    component: _Invoice["default"]
  }, {
    path: 'change-password',
    name: 'simple-menu-change-password',
    component: _Main11["default"]
  }, {
    path: 'regular-table',
    name: 'simple-menu-regular-table',
    component: _Main12["default"]
  }]
}, {
  path: '/top-menu',
  component: _Main3["default"],
  children: [{
    path: '/',
    name: 'top-menu-dashboard-overview',
    component: _Main4["default"]
  }, {
    path: 'inquiry',
    name: 'top-menu-inquiry',
    component: _Main5["default"]
  }, {
    path: 'profile-overview',
    name: 'top-menu-profile-overview',
    component: _Main6["default"]
  }, {
    path: 'update-profile',
    name: 'top-menu-update-profile',
    component: _Main10["default"]
  }, {
    path: 'invoice',
    name: 'invoice',
    component: _Invoice["default"]
  }, {
    path: 'change-password',
    name: 'top-menu-change-password',
    component: _Main11["default"]
  }, {
    path: 'regular-table',
    name: 'top-menu-regular-table',
    component: _Main12["default"]
  }]
}, {
  path: '/login',
  name: 'login',
  component: _Main7["default"]
}, {
  path: '/verifyphone',
  name: 'verifyphone',
  component: _verify_phone["default"]
}, {
  path: '/verifyemail',
  name: 'verifyemail',
  component: _verify_email["default"]
}, {
  path: '/register',
  name: 'register',
  component: _Main8["default"]
}, {
  path: '/error-page',
  name: 'error-page',
  component: _Main9["default"]
}, {
  path: '/:pathMatch(.*)*',
  component: _Main9["default"]
}];
var router = (0, _vueRouter.createRouter)({
  history: (0, _vueRouter.createWebHistory)(process.env.BASE_URL),
  routes: routes,
  scrollBehavior: function scrollBehavior(to, from, savedPosition) {
    return savedPosition || {
      left: 0,
      top: 0
    };
  }
});
router.beforeEach(function (to, from, next) {
  var publicPages = ['/login', '/register', '/error-page', '/contact'];
  var authRequired = !publicPages.includes(to.path);
  var loggedIn = JSON.parse(localStorage.getItem('user'));

  if (loggedIn && Object.prototype.hasOwnProperty.call(loggedIn, 'expiry')) {
    // check of token expired.
    console.log(loggedIn.expiry);

    if (new Date(loggedIn.expiry) < new Date()) {
      _authService["default"].deleteToken();

      next('/login');
    }
  } // trying to access a restricted page + not logged in
  // redirect to login page


  next(authRequired && !loggedIn ? '/login' : null);
});
var _default = router;
exports["default"] = _default;