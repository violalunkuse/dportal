import axios from '../axios'
import { useLocalStorage } from './local-starage-service'

class AuthService {
  login(user) {
    return axios
      .post('patients/auth/login', {
        identifier: user.identifier,
        password: user.password
      })
      .then(response => {
        console.log(
          'service1 login: current lang',
          localStorage.getItem('lang')
        )
        console.log('response.status check', response)
        if (response.data.status === true) {
          console.log(
            'service2 login: current lang',
            localStorage.getItem('lang')
          )
          const { user, expires_in, token } = response.data.payload
          const { saveSession } = useLocalStorage()
          saveSession(user, token, expires_in)
          return response.data
        } else if (response.data.status === false) {
          console.log('login failed else', response.data)
          return response.data
        }
        throw response.data.error
      })
  }

  logout() {
    useLocalStorage().clearSession()
  }

  register(user) {
    // return axios.post(API_URL + 'register', {
    return axios
      .post('patients/auth/register', {
        firstName: user.firstName,
        lastName: user.lastName,
        phone: user.phone,
        address: user.address,
        email: user.email,
        birthDate: user.birthDate,
        password: user.password,
        password_confirmation: user.password_confirmation,
        gender: user.gender
      })
      .then(response => {
        console.log('response.status check', response)
        if (response.data.status === true) {
          return response.data
        }
        throw response.data.error
      })
  }

  verifyPhoneNumber(userId, data) {
    const token = localStorage.getItem('token')
    return axios
      .post('/phoneVerification/' + userId, data, {
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'multipart/form-data',
          'X-Requested-With': 'XMLHttpRequest'
        }
      })
      .then(response => {
        const res = response.data
        localStorage.setItem('user', JSON.stringify(response.data.data.user))
        return { success: res.success, data: res.data, message: res.message }
      })
  }

  verifyEmail(userId, data) {
    const token = localStorage.getItem('token')
    return axios
      .post('/emailVerification/' + userId, data, {
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'multipart/form-data',
          'X-Requested-With': 'XMLHttpRequest'
        }
      })
      .then(response => {
        const res = response.data
        localStorage.setItem('user', JSON.stringify(response.data.data.user))
        // this.$store.commit('auth/update', { user: data, isUpdate: true })
        return { success: res.success, data: res.data, message: res.message }
      })
  }
}

export default new AuthService()
