"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AuthService =
/*#__PURE__*/
function () {
  function AuthService() {
    _classCallCheck(this, AuthService);
  }

  _createClass(AuthService, [{
    key: "login",
    value: function login(user) {
      return _axios["default"] // .post(API_URL + 'login', {
      .post('/auth/login', {
        identifier: user.identifier,
        password: user.password
      }).then(function (response) {
        console.log('response.status check', response.data.status);

        if (response.data.status == true) {
          console.log('response.status inside', response.data.status);
          localStorage.setItem('token', response.data.payload.token);
          localStorage.setItem('loginstatus', response.data.status);
          localStorage.setItem('user', JSON.stringify(response.data.payload.user));
          console.log('token', response.data.payload.token);
          console.log('token user', JSON.stringify(response.data.payload.user)); // localStorage.setItem('phoneverification', JSON.stringify(response.data.data.user.phone_verified))
        } else {
          console.log('response.status inside fail', response.data.status);
          localStorage.setItem('loginfail', response.data.error);
          console.log('fail user', response.data.error); // localStorage.setItem('phoneverification', JSON.stringify(response.data.data.user.phone_verified))
        }

        return response; // return response.data
      });
    }
  }, {
    key: "logout",
    value: function logout() {
      localStorage.removeItem('user');
      localStorage.removeItem('token');
      localStorage.removeItem('phoneverification');
      localStorage.removeItem('loginstatus');
    }
  }, {
    key: "register",
    value: function register(user) {
      // return axios.post(API_URL + 'register', {
      return _axios["default"].post('/auth/register', {
        firstName: user.firstName,
        lastName: user.lastName,
        phone: user.phone,
        address: user.address,
        email: user.email,
        birthDate: user.birthDate,
        password: user.password,
        password_confirmation: user.password_confirmation
      });
    }
  }, {
    key: "verifyPhoneNumber",
    value: function verifyPhoneNumber(userId, data) {
      var token = localStorage.getItem('token');
      return _axios["default"].post('/phoneVerification/' + userId, data, {
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'multipart/form-data',
          'X-Requested-With': 'XMLHttpRequest'
        }
      }).then(function (response) {
        var res = response.data;
        localStorage.setItem('user', JSON.stringify(response.data.data.user));
        return {
          success: res.success,
          data: res.data,
          message: res.message
        };
      });
    }
  }, {
    key: "verifyEmail",
    value: function verifyEmail(userId, data) {
      var token = localStorage.getItem('token');
      return _axios["default"].post('/emailVerification/' + userId, data, {
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'multipart/form-data',
          'X-Requested-With': 'XMLHttpRequest'
        }
      }).then(function (response) {
        var res = response.data;
        localStorage.setItem('user', JSON.stringify(response.data.data.user)); // this.$store.commit('auth/update', { user: data, isUpdate: true })

        return {
          success: res.success,
          data: res.data,
          message: res.message
        };
      });
    }
  }]);

  return AuthService;
}();

var _default = new AuthService();

exports["default"] = _default;