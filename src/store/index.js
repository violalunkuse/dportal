import { createStore } from 'vuex'
import { auth } from './auth'
import main from './main'
import sideMenu from './side-menu'
// import simpleMenu from './simple-menu'
import topMenu from './top-menu'
import langstore from './langstore'
import * as translations from './modules/translations'
const store = createStore({
  modules: {
    main,
    sideMenu,
    // simpleMenu,
    topMenu,
    auth,
    translations,
    langstore
  }
})

export function useStore() {
  return store
}

export default store
