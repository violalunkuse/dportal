"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var state = function state() {
  return {
    menu: [{
      icon: 'HomeIcon',
      pageName: 'side-menu-dashboard-overview',
      title: 'Home'
    }, {
      icon: 'CalendarIcon',
      pageName: 'side-menu-appointments',
      title: 'Appointments'
    }, {
      icon: 'MessageSquareIcon',
      pageName: 'side-menu-inquiry',
      title: 'Messages'
    }, {
      icon: 'CreditCardIcon',
      pageName: 'side-menu-invoice',
      title: 'Invoices'
    }, {
      icon: 'BellIcon',
      pageName: 'side-menu-notification',
      title: 'Notifications'
    }, {
      icon: 'UserIcon',
      pageName: 'side-menu-profile-overview',
      title: 'Profile'
    }, {
      icon: 'ToolIcon',
      pageName: 'side-menu-settings',
      title: 'Settings'
    }]
  };
}; // getters


var getters = {
  menu: function menu(state) {
    return state.menu;
  }
}; // actions

var actions = {}; // mutations

var mutations = {};
var _default = {
  namespaced: true,
  state: state,
  getters: getters,
  actions: actions,
  mutations: mutations
};
exports["default"] = _default;