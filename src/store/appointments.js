import AuthService from '../service/appointments-service'

export default class AppointmentsService {
    static async fetchAppointments(url, data, cancelToken) {
      try {
        const response = await customAxios.post(url);
        axios
              .post(
                url,
                data,
                { cancelToken: cancelToken }
              )
        let data = response.data;
        console.log(data);
        if (data.status === 'SUCCESS') {
          return { success: true, data: data.payload, message: 'Appointments loaded' };
        }
        return { success: false, data: {}, message: data.payload };
      } catch (err) {
        return { success: false, data: {}, message: `${err}` };
      }
    }
}