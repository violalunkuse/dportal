const intersectionObserverMock = () => ({
    observe: () => null
})
window.IntersectionObserver = vitest.fn().mockImplementation(intersectionObserverMock);