import { describe, expect, it, test, vi } from "vitest"

import { mount } from '@vue/test-utils'
import LoginForm from '../src/views/login/Main.vue';
import axios from "../src/axios"

import authService from "../src/service/auth-service"
import { useLocalStorage } from "../src/service/local-starage-service"
import { useStore } from '@/store'


vi.mock('../src/axios')
vi.mock('../src/utils/cash')

let methods = {
  handleSignIn: vi.fn()
}
const data = {
    identifier: 'amalia@gmail.com',
    password: 'Amalia@.1'
}
const password = 'password'

  const mockRoute = {
    
  }
  const mockRouter = {
    push: vi.fn()
  }
  
  
const responseData = {
    message: "Login Successful",
    status: true, payload: {
        user: {
            email: 'amalia@gmail.com',
            name: 'Violah'
        },
        token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3M'
    }
}

const spy = vi.spyOn(LoginForm.methods, 'handleSignIn');

const { user } = useLocalStorage()
describe('login process', () => {

    test('successful login process ', async () => {
        const $store = {
          dispatch: vi.fn().mockImplementation(() => Promise.resolve()),
            state: {
                auth:{status: { loggedIn: true }}
              },
             
              
            commit: vi.fn()
          }

          const wrapper = mount(LoginForm, {
            data() {
              return {
                identifier: 'amalia@gmail.com',
                password: 'Amalia@.1',
                
         }
            },
            global: {
                mocks: {
                  $store,
                  $route: mockRoute,
                  $router: mockRouter,
                  methods
                },
                
                    stubs: {
                        DarkModeSwitcher: true
                    }
                
              }
          });
        let identifierInput = wrapper.find("#identifier_field");
    expect(identifierInput .exists()).toBe(true);


    await wrapper.find('#identifier_field').setValue('amalia@gmail.com')
    await wrapper.find('form').trigger('submit.prevent')
    wrapper.vm.handleSignIn();
    expect(spy).toHaveBeenCalled();
      // expect(handleSignIn).toHaveBeenCalled()
   



        axios.post.mockResolvedValue({ data: responseData })
        const res = await authService.login(data);

        expect(axios.post).toBeCalled()

        expect(res.status).equal(true)
        expect(res.status).toBeTruthy();

        expect(res.message).equal('Login Successful')
        expect(res.payload.token).equal('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3M')
        const { user, token } = useLocalStorage()

    

    })

    test('failed login process ', async () => {
    const mockResponseData = {
      message: "Check your Credentials",
      status: false, payload: {
          
          token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3M'
      }
  }
  const mockData = {
    identifier: 'amalia@gmail.com',
    password: 'Amalia@.111'
}
      const $store = {
          state: {
              auth:{status: { loggedIn: false}}
            },
          commit: vi.fn()
        }

        const wrapper = mount(LoginForm, {
          data() {
            return {
              identifier: 'amalia@gmail.com',
              password: 'Amalia@.1111',
              
       }
          },
          global: {
              mocks: {
                $store,
                $route: mockRoute,
                $router: mockRouter
              },
              
                  stubs: {
                      DarkModeSwitcher: true
                  }
              
            }
        });
        
      let identifierInput = wrapper.find("#identifier_field");
  expect(identifierInput.exists()).toBe(true);
  let passwordInput = wrapper.find("#password_field");
  expect(passwordInput.exists()).toBe(true);
  let submitButton = wrapper.find("#submit_button");
  expect(submitButton .exists()).toBe(true);
 
      axios.post.mockResolvedValue({ data: mockResponseData })
      const res = await authService.login(mockData);
      expect(mockData.identifier).toBe('amalia@gmail.com')
      expect(mockData.password).toBe('Amalia@.111')
      expect(axios.post).toBeCalled()
      expect(res.status).equal(false)
    expect(res.message).equal('Check your Credentials')


  

  })

  

})