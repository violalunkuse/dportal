// import { shallow } from 'enzyme';
import axios from '../src/axios'
import { mount } from '@vue/test-utils'
import Invoices from '../src/views/invoices/invoices/InvoiceContainer.vue'
import InvoiceService from '../src/service/invoices-service'
import './intersectionObserverMock'
vi.mock('axios')
describe('should handle retrieving Invoice display', () => {
  it('successfully retrieved invoices', async () => {
    const mockUrl = 'patients/invoices/all'
    const mockData = {}
    const mockCancelToken = ''

    const mockResponseData = {
      message: '',
      payload: {
        current_page: 1,
        data: [
          {
            id: 256,
            patient_id: 26,
            doctor_id: 3,
            facility_id: 1,
            invoice_id: 'MDT-00000256',
            service_type: 3,
            services: [78, 78, 90],
            prices: '35.5,56.9,100.5',
            status: 1,
            invoice_type: 0,
            paidamount: '66.90',
            balance_due: '100.50',
            due_date: null,
            internal_notes: null,
            vat: 18,
            total_with_vat: '790.00',
            deleted_at: null,
            created_at: '2023-02-09T09:55:13.000000Z',
            updated_at: '2023-02-09T09:55:13.000000Z',
            payment_mode: null,
            description: null,
            doctors: null,
            treatments: [
              {
                id: 78,
                treatment: 'Electronic length determination',
                code: 'E85',
                price: '15.07',
                facility_id: 1,
                treatment_category: 13,
                subcategory: 30,
                tooth_sections: null,
                deleted_at: null,
                created_at: '2022-04-07T15:33:37.000000Z',
                updated_at: '2022-04-07T15:33:37.000000Z',
                pivot: {
                  invoice_id: 256,
                  treatment_id: 78
                }
              },
              {
                id: 78,
                treatment: 'Electronic length determination',
                code: 'E85',
                price: '15.07',
                facility_id: 1,
                treatment_category: 13,
                subcategory: 30,
                tooth_sections: null,
                deleted_at: null,
                created_at: '2022-04-07T15:33:37.000000Z',
                updated_at: '2022-04-07T15:33:37.000000Z',
                pivot: {
                  invoice_id: 256,
                  treatment_id: 78
                }
              },
              {
                id: 90,
                treatment:
                  'Surcharge for sealing with Mineral Trioxide Aggregate (MTA) or a comparable bioceramic material',
                code: 'E63',
                price: '45.22',
                facility_id: 1,
                treatment_category: 13,
                subcategory: 30,
                tooth_sections: null,
                deleted_at: null,
                created_at: '2022-04-07T15:33:38.000000Z',
                updated_at: '2022-04-07T15:33:38.000000Z',
                pivot: {
                  invoice_id: 256,
                  treatment_id: 90
                }
              }
            ]
          },
          {
            id: 257,
            patient_id: 26,
            doctor_id: 4,
            facility_id: 1,
            invoice_id: 'MDT-00000257',
            service_type: 2,
            services: [78, 78, 90],
            prices: '35.5,56.9,100.5',
            status: 0,
            invoice_type: 1,
            paidamount: '66.90',
            balance_due: '100.50',
            due_date: null,
            internal_notes: null,
            vat: 18,
            total_with_vat: '790.00',
            deleted_at: null,
            created_at: '2023-02-09T09:55:13.000000Z',
            updated_at: '2023-02-09T09:55:13.000000Z',
            payment_mode: null,
            description: null,
            doctors: null,
            treatments: [
              {
                id: 78,
                treatment: 'Electronic length determination',
                code: 'E85',
                price: '15.07',
                facility_id: 1,
                treatment_category: 13,
                subcategory: 30,
                tooth_sections: null,
                deleted_at: null,
                created_at: '2022-04-07T15:33:37.000000Z',
                updated_at: '2022-04-07T15:33:37.000000Z',
                pivot: {
                  invoice_id: 257,
                  treatment_id: 78
                }
              },
              {
                id: 78,
                treatment: 'Electronic length determination',
                code: 'E85',
                price: '15.07',
                facility_id: 1,
                treatment_category: 13,
                subcategory: 30,
                tooth_sections: null,
                deleted_at: null,
                created_at: '2022-04-07T15:33:37.000000Z',
                updated_at: '2022-04-07T15:33:37.000000Z',
                pivot: {
                  invoice_id: 257,
                  treatment_id: 78
                }
              },
              {
                id: 90,
                treatment:
                  'Surcharge for sealing with Mineral Trioxide Aggregate (MTA) or a comparable bioceramic material',
                code: 'E63',
                price: '45.22',
                facility_id: 1,
                treatment_category: 13,
                subcategory: 30,
                tooth_sections: null,
                deleted_at: null,
                created_at: '2022-04-07T15:33:38.000000Z',
                updated_at: '2022-04-07T15:33:38.000000Z',
                pivot: {
                  invoice_id: 257,
                  treatment_id: 90
                }
              }
            ]
          }
        ],
        first_page_url:
          'http://127.0.0.1:8000/api/patients/invoices/all?page=1',
        from: 1,
        last_page: 15,
        last_page_url:
          'http://127.0.0.1:8000/api/patients/invoices/all?page=15',

        status: true,
        msg: 'success',
        statusCode: 200,
        links: [
          {
            url: null,
            label: '&laquo; Previous',
            active: false
          },
          {
            url: 'http://127.0.0.1:8000/api/patients/invoices/all?page=1',
            label: '1',
            active: true
          }
        ],
        next_page_url: 'http://127.0.0.1:8000/api/patients/invoices/all?page=2',
        path: 'http://127.0.0.1:8000/api/patients/invoices/all',
        per_page: 20
      },
      status: true
    }

    const wrapper = mount(Invoices)
    let searchKeyword = wrapper.find('#searchID')
    expect(searchKeyword.exists()).toBe(true)
    // expect(wrapper.html()).toContain(mockPaymentData.paidAmount)
    axios.post.mockResolvedValue({ data: mockResponseData })
    const res = await InvoiceService.fetchInvoices(
      mockUrl,
      mockData,
      mockCancelToken
    )
    console.log('success displayed invoices', res)
    expect(res.data.status).equal(true)
    expect(res.data.url).equal(mockResponseData.payload.url)
    expect(res.data.statusCode).equal(mockResponseData.payload.statusCode)
    expect(res.data.msg).equal(mockResponseData.payload.msg)
    expect(res.data.data).equal(mockResponseData.payload.data)
    expect(res.data.data).toHaveLength(2)
  })

  //  search_word: this.search_term
  it('Unsuccessfull retrieved invoices', async () => {
    const mockUrl = 'patients/invoices/all'
    const mockData = {}
    const mockResponseData = {
      status: false,
      error_code: 401
    }

    const wrapper = mount(Invoices)

    let searchKeyword = wrapper.find('#searchID')
    expect(searchKeyword.exists()).toBe(true)
    axios.post.mockResolvedValue({ data: mockResponseData })
    const res = await InvoiceService.fetchInvoices(mockUrl, mockData)
    console.log('failed displayed invoices', res)
    expect(res.data.status).equal(false)
    expect(res.data.status).equal(mockResponseData.status)
    expect(res.data.error_code).equal(mockResponseData.error_code)
  })
})
