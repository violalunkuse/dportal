import { describe, expect, it, test, vi } from "vitest"
import axios from "../src/axios"
import authService from "../src/service/auth-service"

vi.mock('axios')

const data = {
    firstName: 'Randy',
    lastName: 'Lee',
    phone: '31778099900',
    address: 'Kampala',
    email: 'amalia@gmail.com',
    birthDate: '23-01-2003',
    password: 'Amalia@.1d',
    password_confirmation: 'Amalia@.1',
    gender: 'Male'
}

const responseData = {
    message: "Patient registration successfully",
    status: true, payload: {
        user: { BSN: null,birth_date: "11-09-2017",city: "Kampala", country: "Netherlands",first_name: "Randy",gender: "Male", id: 26,  last_name: "Lee" },
        token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3M'
    }
}


describe('Registration process', () => {

    test('Register endpoint ', async () => {
        axios.post.mockResolvedValue({ data: responseData })


        const res = await authService.register(data);

        expect(axios.post).toBeCalled()

        expect(res.status).equal(true)
        expect(res.message).equal('Patient registration successfully')
        expect(res.payload.token).equal('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3M')
        expect(res.payload.user).contains({ BSN: null,birth_date: "11-09-2017",city: "Kampala", country: "Netherlands",first_name: "Randy",gender: "Male", id: 26,  last_name: "Lee" })

   
  

    })

    test('Verify if a user will be able to register with a valid first name', ()=>{
        expect(data.firstName).toBe('Randy')
    })

    test('Verify if a user will be able to register with a valid password', ()=>{
        expect(data.lastName).toBe('Lee')
    })
})