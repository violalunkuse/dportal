import { describe, test, expect, it, vi, beforeAll } from "vitest";
import moment from "moment";
import axios from "../src/axios"
import AppointmentsService from "../src/service/appointments-service"



vi.mock('axios')
let search_term=null
    let year_range = null
    let filterStatus= null
    let date_range=null
    let month_range=null
let appointments = [

    {
        "id": 410,
        "facility_id": 1,
        "patient_id": 26,
        "type_id": 0,
        "status_id": 1,
        "frequency_id": 1,
        "frequency_value": 1,
        "source_id": 2,
        "treatment_type_id": null,
        "treatment_id": null,
        "parent_id": null,
        "period_id": 3,
        "interval": 10,
        "appointment_type_id": 1,
        "doctors": [
            3,
            4
        ],
        "date": "18-02-2023",
        "slots": [
            {
                "start-time": "19:30",
                "end-time": "19:40"
            }
        ],
        "comments": null,
        "material_name": null,
        "material_date": null,
        "servingtime": null,
        "checkin_time": null,
        "deleted_at": null,
        "created_at": "2023-02-24T08:03:31.000000Z",
        "updated_at": "2023-02-24T08:03:31.000000Z",
        "appointment_code": null,
        "employee_id": null,
        "department_id": 2,
        "UUID": null,
        "watch_resp": null,
        "treatment_plan_id": null,
        "phase_id": null,
        "is_recallable": 0,
        "medical_questions_answer_id": null,
        "is_qtns_requested": null,
        "status": {
            "id": 1,
            "status": "Confirmed",
            "created_at": "2022-12-01T16:26:55.000000Z",
            "updated_at": "2022-12-01T16:26:55.000000Z"
        },
        "appointment_type": {
            "id": 1,
            "title": "Children Care",
            "code": "APTCC",
            "color": "#eeefff",
            "agenda_interval": 10,
            "for_web": 0,
            "doctors": [
                1,
                2
            ],
            "week_days": [
                1,
                4
            ],
            "start_time": "10:00",
            "end_time": "12:00",
            "start_date": "21-04-2022",
            "end_date": "04-08-2022",
            "notes": "some simple notes here",
            "facility_id": 1,
            "deleted_at": null,
            "created_at": "2022-12-01T16:26:54.000000Z",
            "updated_at": "2022-12-01T16:26:54.000000Z"
        },
        "treatment_type": null,
        "source": {
            "id": 2,
            "source": "Phone",
            "created_at": "2022-12-01T16:26:55.000000Z",
            "updated_at": "2022-12-01T16:26:55.000000Z"
        },
        "employees": []
    },
    {
        "id": 409,
        "facility_id": 1,
        "patient_id": 26,
        "type_id": 0,
        "status_id": 1,
        "frequency_id": 1,
        "frequency_value": 1,
        "source_id": 2,
        "treatment_type_id": null,
        "treatment_id": null,
        "parent_id": null,
        "period_id": 3,
        "interval": 1,
        "appointment_type_id": 6,
        "doctors": [
            3,
            4
        ],
        "date": "24-08-2023",
        "slots": [
            {
                "start-time": "19:59",
                "end-time": "20:00"
            }
        ],
        "comments": null,
        "material_name": null,
        "material_date": null,
        "servingtime": null,
        "checkin_time": null,
        "deleted_at": null,
        "created_at": "2023-02-24T05:48:27.000000Z",
        "updated_at": "2023-02-24T05:48:27.000000Z",
        "appointment_code": null,
        "employee_id": null,
        "department_id": 2,
        "UUID": null,
        "watch_resp": null,
        "treatment_plan_id": null,
        "phase_id": null,
        "is_recallable": 0,
        "medical_questions_answer_id": null,
        "is_qtns_requested": "true",
        "status": {
            "id": 1,
            "status": "Confirmed",
            "created_at": "2022-12-01T16:26:55.000000Z",
            "updated_at": "2022-12-01T16:26:55.000000Z"
        },
        "appointment_type": {
            "id": 6,
            "title": "Perio Check",
            "code": "PC001",
            "color": "#ff2e38",
            "agenda_interval": 1,
            "for_web": 0,
            "doctors": [
                3
            ],
            "week_days": [
                1,
                2,
                3,
                4
            ],
            "start_time": "03:00",
            "end_time": "18:00",
            "start_date": "28-12-2022",
            "end_date": "28-01-2023",
            "notes": "Testing",
            "facility_id": 1,
            "deleted_at": null,
            "created_at": "2022-12-28T15:01:33.000000Z",
            "updated_at": "2022-12-28T15:03:40.000000Z"
        },
        "treatment_type": null,
        "source": {
            "id": 2,
            "source": "Phone",
            "created_at": "2022-12-01T16:26:55.000000Z",
            "updated_at": "2022-12-01T16:26:55.000000Z"
        },
        "employees": []
    },


]

let data = {}
const cancelToken = ''
const url = 'http://127.0.0.1:8000/api/patients/appointments/all-paginated'
const responseData = {
    message: "Appointments loaded",
    success: true,
    payload: {
        current_page: 1,
        data: appointments,
        to: 2,
        total: 2
    }
}

// describe('appointment retrieving process', () => {
//     test('get appointment endpoint ', async () => {
//         axios.post.mockResolvedValue({ data: responseData })
//         const res = await AppointmentsService.fetchAppointments(url, data, cancelToken);
//         console.log('pppppp  res', res.data.data)

//         expect(axios.post).toBeCalled()

//         expect(res.success).equal(true)
//         expect(res.success).toBeTruthy();

//         expect(res.message).equal('Appointments loaded')
// expect(res.payload.token).equal('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3M')
// const { user, token } = useLocalStorage()



//     })


// })

describe("search appointments by keyword", () => {
    // keyword: Phone
    test("keyword available", async () => {
        // data = { search_word: this.search_term }
        axios.post.mockResolvedValue({ data: responseData })
        const res = await AppointmentsService.fetchAppointments(url, data, cancelToken);
        expect(axios.post).toBeCalled()

        expect(res.success).equal(true)
        expect(res.success).toBeTruthy();
        let appointments = res.data.data




        const searchByAppointmentKeyWord = (search_word) => {
            return appointments.filter(
                appointment =>
                    appointment?.treatment_type?.title
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment.treatment_type?.code
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment.status?.status
                        .toLowerCase()
                        .includes(search_word?.toLowerCase()) ||
                    appointment?.appointment_type?.title
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment?.doctors[0]?.first_name
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment?.doctors[0]?.last_name
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment?.patient?.first_name
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment?.patient?.last_name
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment.source?.source
                        .toLowerCase()
                        .includes(search_word?.toLowerCase())
            );
        };

      

        expect(searchByAppointmentKeyWord("Confirmed")).toBeTruthy()
        expect(searchByAppointmentKeyWord("Confirmed").length).toBe(2);
        expect(searchByAppointmentKeyWord("Confirmed").length).toBeLessThan(3);

    });

    test("keyword not available", async () => {
        axios.post.mockResolvedValue({ data: responseData })
        const res = await AppointmentsService.fetchAppointments(url, data, cancelToken);
        expect(axios.post).toBeCalled()

        expect(res.success).equal(true)
        expect(res.success).toBeTruthy();

        expect(res.message).equal('Appointments loaded')
        let appointments = res.data.data


        const searchByAppointmentKeyWord = (search_word) => {
            return appointments.filter(
                appointment =>
                    appointment?.treatment_type?.title
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment.treatment_type?.code
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment.status?.status
                        .toLowerCase()
                        .includes(search_word?.toLowerCase()) ||
                    appointment?.appointment_type?.title
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment?.doctors[0]?.first_name
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment?.doctors[0]?.last_name
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment?.patient?.first_name
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment?.patient?.last_name
                        ?.toLowerCase()
                        ?.includes(search_word?.toLowerCase()) ||
                    appointment.source?.source
                        .toLowerCase()
                        .includes(search_word?.toLowerCase())
            );
        };

     

        expect(searchByAppointmentKeyWord("Confirmed")).toBeTruthy()
        expect(searchByAppointmentKeyWord("Pending").length).toBe(0);
        expect(searchByAppointmentKeyWord("mmmmmm").length).toBe(0);
        expect(searchByAppointmentKeyWord("Completed").length).toBeLessThan(3);


    });


});


// filter  appointments by status
describe("search appointments by status ", () => {
    // status_id: 2: Pending, 1:Confirmed, 3:Waiting, 4:closed, 5:canceled, 6:missed, 7:serving
    // available
    test("appointments with this status  available", async () => {
        axios.post.mockResolvedValue({ data: responseData })
        const res = await AppointmentsService.fetchAppointments(url, data, cancelToken);
        expect(axios.post).toBeCalled()

        expect(res.success).equal(true)
        expect(res.success).toBeTruthy();
        let appointments = res.data.data


        const filterAppointmentsStatus = (status) => {

            return appointments.filter(
                appointment => appointment.status_id == status
            );
        };

        // Status confirmed is available
        expect(filterAppointmentsStatus(1)).toBeTruthy()
        expect(filterAppointmentsStatus(1).length).toBe(2);
        expect(filterAppointmentsStatus(1).length).toBeLessThan(3);
    });

//  not available
    test("no appointment with this status", async () => {
        axios.post.mockResolvedValue({ data: responseData })
        const res = await AppointmentsService.fetchAppointments(url, data, cancelToken);
        expect(axios.post).toBeCalled()

        expect(res.success).equal(true)
        expect(res.success).toBeTruthy();

        
        let appointments = res.data.data


        const filterAppointmentsStatus = (status) => {

            return appointments.filter(
                appointment => appointment.status_id == status
            );
        };


        expect(filterAppointmentsStatus(2)).toBeTruthy()
        expect(filterAppointmentsStatus(2).length).toBe(0);
        expect(filterAppointmentsStatus(3).length).toBe(0);
        expect(filterAppointmentsStatus(4).length).toBe(0);
        expect(filterAppointmentsStatus(5).length).toBe(0);
        expect(filterAppointmentsStatus(6).length).toBe(0);
        expect(filterAppointmentsStatus(7).length).toBe(0);
        expect(filterAppointmentsStatus(2).length).toBeLessThan(3);
    });



});










describe("filter appointments by date range", () => {
    // date range
    test("Appointments available in this range", async () => {
        axios.post.mockResolvedValue({ data: responseData })
        const res = await AppointmentsService.fetchAppointments(url, data, cancelToken);
        expect(axios.post).toBeCalled()

        expect(res.success).equal(true)
        expect(res.success).toBeTruthy();
        let appointments = res.data.data




//  filter appointments by date range
const filterAppointmentsDate = (startDate, endDate) => {
 
    return appointments.filter(appointment =>
        moment(
          `${appointment?.date} ${appointment?.slots[0]["start-time"]}`,
          "DD-MM-YYYY HH:mm"
        ).isBetween(
          moment(startDate, "DD-MM-YYYY HH:mm"),
          moment(endDate, "DD-MM-YYYY HH:mm")
        )
      );
  };

// dates available 24-02-2023,  24-04-2023
const startDate = moment("01-01-2023").format("DD-MM-YYYY HH:mm");
const endDate = moment("01-06-2023").format("DD-MM-YYYY HH:mm");

// const filterappointmentsDate = filterAppointmentsDate(startDate, endDate);

// expect(filterappointmentsDate).toBeTruthy()

// expect(filterappointmentsDate.length).toBe(7);

// expect(filterappointmentsDate.length).toBeLessThan(12);

    });


    test(" No appointment is available in this range", async () => {
        axios.post.mockResolvedValue({ data: responseData })
        const res = await AppointmentsService.fetchAppointments(url, data, cancelToken);
        expect(axios.post).toBeCalled()

        expect(res.success).equal(true)
        expect(res.success).toBeTruthy();
        let appointments = res.data.data

//  filter appointments by date range
// const filterAppointmentsDate = (startDate, endDate) => {
 
//     return appointments.filter(appointment =>
//         moment(
//           `${appointment?.date} ${appointment?.slots[0]["start-time"]}`,
//           "DD-MM-YYYY HH:mm"
//         ).isBetween(
//           moment(startDate, "DD-MM-YYYY HH:mm"),
//           moment(endDate, "DD-MM-YYYY HH:mm")
//         )
//       );
//   };

// dates available 24-02-2023,  24-04-2023
//   const startDate = moment("01-08-2023").format("DD-MM-YYYY HH:mm");
//   const endDate = moment("01-12-2023").format("DD-MM-YYYY HH:mm");

//         const filterappointmentsDate = filterAppointmentsDate(startDate, endDate);
    
//         expect(filterAppointmentsDate(startDate, endDate)).toBeTruthy()

//         expect(filterAppointmentsDate(startDate, endDate).length).toBe(0);

//         expect(filterAppointmentsDate(startDate, endDate).length).toBeLessThan(3);

    });


});


//  filter appointments by month range
const filterAppointmentsMonthRange = (date) => {

    const appointmentMonth = moment(
        new Date(date)
    ).format("MM-YYYY");

    return appointments.filter(
        appointment =>
            moment(appointment?.date, "DD-MM-YYYY").format("MM-YYYY") ===
            appointmentMonth
    );
};


describe("filter appointments by month: appointments available within that month", () => {
    // date range
    test(" filter appointments by month: appointments available within this month", () => {

        const date = moment("11-03-2021").format("DD-MM-YYYY HH:mm");

        // const filterappointmentsMonthRange = filterAppointmentsMonthRange(date);

        // expect(filterappointmentsMonthRange).toBeTruthy()

        // expect(filterappointmentsMonthRange.length).toBe(1);

        // expect(filterappointmentsMonthRange.length).toBeLessThan(12);

    });


});


//  filter appointments by year range
const filterAppointmentsYearRange = (appointmentYear) => {
    return appointments.filter(
        appointment =>
            moment(appointment?.date, "DD-MM-YYYY").format("YYYY") ===
            appointmentYear
    );



};

describe("filter appointments by year", () => {
    // year range

    
    // let search_term=""
    // let year_range = ""
    // let filterStatus= ""
    // let date_range=""
    // let month_range=""

    // search_word: this.search_term
    // status: this.filterStatus
    // year: this.year_range
    // date_range: this.date_range
    // month_range: this.month_range 

    

    test(" appointments filtered by year range", async () => {
        let year = moment(new Date("11-03-2023")).format("YYYY")
       
        filterStatus=null
        search_term=null
        year_range = year
        date_range=null
        month_range=null

        data = { year: year_range  }
        axios.post.mockResolvedValue({ data: responseData })
        const res = await AppointmentsService.fetchAppointments(url, data, cancelToken);
        expect(axios.post).toBeCalled()
        // let appointments = res.data.data

        expect(filterStatus).toBe(null)
        expect(search_term).toBe(null)
        expect(date_range).toBe(null)
        expect( month_range).toBe(null)
        expect(typeof year).toBe('string');
        expect(Array.isArray(['data'])).toBe(true);
        expect(res.success).equal(true)
        expect(res.success).toBeTruthy();
        expect(res.message).equal('Appointments loaded')
        let appointments = res.data.data
        expect(Array.isArray([appointments])).toBe(true);


    });


});


