// import { shallow } from 'enzyme';
import axios from "../src/axios"
import { mount } from '@vue/test-utils'
import PaymentForm from '../src/components/payments/Pay.vue';
import InvoiceService from '../src/service/invoices-service'
vi.mock('axios')
describe('should handle payment form submission', () => {
  it('successfull submission', async () => {
    const mockUrl = 'patients/payments/process'
    const mockPaymentData = { paidAmount: 100.00 };
    const mockResponseData = {
      message: '',
      payload: { url: 'https://www.mollie.com/checkout/credit-card/embedded/3jJizdx9vQ', status: true, msg: 'success', statusCode: 200, }
    };

    const wrapper = mount(PaymentForm, {
      data() {
        return {
          selected_appointment_type: '2',


        }
      }
    });
    let amountInput = wrapper.find("#amount");
    expect(amountInput.exists()).toBe(true);
    expect(wrapper.html()).toContain(mockPaymentData.paidAmount)
    axios.post.mockResolvedValue({ data: mockResponseData })
    const res = await InvoiceService.makePayment(mockUrl, mockPaymentData);
    expect(res.data.status).equal(true)
    expect(res.data.url).equal(mockResponseData.payload.url)
    expect(res.data.statusCode).equal(mockResponseData.payload.statusCode)
    expect(res.data.msg).equal(mockResponseData.payload.msg)
    expect(wrapper.html()).toContain(res.data.msg)


  });



  it('Unsuccessfull submission', async () => {
    const mockUrl = 'patients/payments/process'
    const mockPaymentData = { paidAmount: 0.00 };
    const mockResponseData = {
      status: false,
      error: "[2023-03-02T13:04:03+0000] Error executing API call (422: Unprocessable Entity): The amount is lower than the minimum. Field: amount. Documentation: https://docs.mollie.com/overview/handling-errors"
    };

    const wrapper = mount(PaymentForm, {
      data() {
        return {
          selected_appointment_type: '2',
        }
      }
    });
    let amountInput = wrapper.find("#amount");
    expect(amountInput.exists()).toBe(true);
    expect(wrapper.html()).toContain(mockPaymentData.paidAmount)
    axios.post.mockResolvedValue({ data: mockResponseData })
    const res = await InvoiceService.makePayment(mockUrl, mockPaymentData);
    expect(res.status).equal(false)
    expect(res.status).equal(mockResponseData.status)


  });
});
