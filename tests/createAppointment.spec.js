import { describe, expect, it, test, vi } from "vitest"

import { mount } from '@vue/test-utils'
import createAppointment from '../src/views/appointments/AddAppointment.vue'
import axios from "../src/axios"
import { useLocalStorage } from "../src/service/local-starage-service"
import AppointmentsService from "../src/service/appointments-service";
import TomSelect from 'tom-select'

vi.mock('axios')

const data = {
  appointmentTypeId: "1",
comments: "okay",
date: "2023-03-01",
doctors: [],
facility_id: "1",
frequencyId: "1",
interval: "10",
periodId: "3",
slots: [{"start-time": "07:50", "end-time": "08:00"}],
sourceId: 2,
statusId: 2}
const password = 'password'

const responseData = {
  message: "Appointment Created Successfully",
  status: true, 
    payload: {
      "id": 409,
      "facility_id": 1,
      "patient_id": 26,
      "type_id": 0,
      "status_id": 1,
      "frequency_id": 1,
      "frequency_value": 1,
      "source_id": 2,
      "treatment_type_id": null,
      "treatment_id": null,
      "parent_id": null,
      "period_id": 3,
      "interval": 1,
      "appointment_type_id": 6,
      "doctors": [
          3,
          4
      ],
      "date": "24-08-2023",
      "slots": [
          {
              "start-time": "19:59",
              "end-time": "20:00"
          }
      ],
      "comments": null,
      "material_name": null,
      "material_date": null,
      "servingtime": null,
      "checkin_time": null,
      "deleted_at": null,
      "created_at": "2023-02-24T05:48:27.000000Z",
      "updated_at": "2023-02-24T05:48:27.000000Z",
      "appointment_code": null,
      "employee_id": null,
      "department_id": 2,
      "UUID": null,
      "watch_resp": null,
      "treatment_plan_id": null,
      "phase_id": null,
      "is_recallable": 0,
      "medical_questions_answer_id": null,
      "is_qtns_requested": "true",
      "status": {
          "id": 1,
          "status": "Confirmed",
          "created_at": "2022-12-01T16:26:55.000000Z",
          "updated_at": "2022-12-01T16:26:55.000000Z"
      },
      "appointment_type": {
          "id": 6,
          "title": "Perio Check",
          "code": "PC001",
          "color": "#ff2e38",
          "agenda_interval": 1,
          "for_web": 0,
          "doctors": [
              3
          ],
          "week_days": [
              1,
              2,
              3,
              4
          ],
          "start_time": "03:00",
          "end_time": "18:00",
          "start_date": "28-12-2022",
          "end_date": "28-01-2023",
          "notes": "Testing",
          "facility_id": 1,
          "deleted_at": null,
          "created_at": "2022-12-28T15:01:33.000000Z",
          "updated_at": "2022-12-28T15:03:40.000000Z"
      },
      "treatment_type": null,
      "source": {
          "id": 2,
          "source": "Phone",
          "created_at": "2022-12-01T16:26:55.000000Z",
          "updated_at": "2022-12-01T16:26:55.000000Z"
      },
      "employees": []
    
  
  }
}



describe('Appointment creation process', () => {
  test('component should render correct contents', async () => {
    const wrapper = mount(createAppointment, {
      data() {
        return {
          selected_appointment_type: '2',
          appointment_date: '2023-02-28',
          newStartTime: '15:00',
          newEndTime: '15:20',
          comment: 'okay',


        }
      }
    })

    expect(wrapper.text()).toContain('Scheduling Assistant')
    expect(wrapper.text()).toContain('Select appointment type')

    let appointmentTypeSelect = wrapper.find("#appointment_type_select");
    expect(appointmentTypeSelect.exists()).toBe(true);
    let commentTextArea = wrapper.find("#comment-text-area");
    expect(commentTextArea.exists()).toBe(true);
    
    let datePicker = wrapper.find("#date_input");
    expect(datePicker.exists()).toBe(true);


    expect(wrapper.html()).toContain('2')
    expect(wrapper.html()).toContain('2023-02-28')
    expect(wrapper.html()).toContain('15:00')
    expect(wrapper.html()).toContain('15:20')

    wrapper.find('form').trigger('submit')
    expect(wrapper.emitted()).toHaveProperty('submit')


  })

  test('create appointment endpoint ', async () => {
    axios.post.mockResolvedValue({ data: responseData })


    const res = await AppointmentsService.createAppointments(data);

    expect(axios.post).toBeCalled()
    expect(res.status).equal(true)
    expect(res.message).equal('Appointment Created Successfully')
    // expect(res.payload).contains({ BSN: null,birth_date: "11-09-2017",city: "Kampala", country: "Netherlands",first_name: "Randy",gender: "Male", id: 26,  last_name: "Lee" })




})

})
