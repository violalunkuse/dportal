// import { shallow } from 'enzyme';
import axios from "../src/axios"
import { mount} from '@vue/test-utils'
import Question from '../src/views/appointments/appointments/answer_dental_questions.vue';
import QuestionService from '../src/service/questions-service'
import './intersectionObserverMock';
vi.mock('axios')
describe('should handle retrieving questions', () => {
    it('successfully retrieved questions', async () => {
        const mockUrl = '/v2/questions/all/v2'
        const mockData = {};
        const mockCancelToken = ''
        
        const mockResponseData =
        {
            "status": "SUCCESS",
            "payload": [
                {
                    "id": 15,
                    "question": "Have you ever been to our hospital?",
                    "type": "closed",
                    "enabled": 1,
                    "created_by": null,
                    "created_at": "2023-02-23T08:35:18.000000Z",
                    "updated_at": "2023-02-23T08:36:19.000000Z",
                    "sub_questions": [
                        {
                            "id": 6,
                            "question": "When and why?",
                            "type": "open",
                            "created_by": null,
                            "medical_question_id": 15
                        }
                    ]
                },
                {
                    "id": 14,
                    "question": "Do you suffer from a heart murmur, cardiac valve/ventricle problems?",
                    "type": "closed",
                    "enabled": 1,
                    "created_by": null,
                    "created_at": "2023-02-20T13:05:25.000000Z",
                    "updated_at": "2023-02-23T08:30:26.000000Z",
                    "sub_questions": []
                },
                {
                    "id": 13,
                    "question": "Did you suffer from a heart attack or cardiac arrest?",
                    "type": "closed",
                    "enabled": 1,
                    "created_by": null,
                    "created_at": "2023-02-20T12:54:46.000000Z",
                    "updated_at": "2023-02-20T12:54:46.000000Z",
                    "sub_questions": [
                        {
                            "id": 5,
                            "question": "Did you experience any complications afterwards?",
                            "type": "closed",
                            "created_by": null,
                            "medical_question_id": 13
                        }
                    ]
                },
                {
                    "id": 12,
                    "question": "Do you experience any pain or pressure in your chest during mild effort (angina pectoris)?",
                    "type": "closed",
                    "enabled": 1,
                    "created_by": null,
                    "created_at": "2023-02-20T12:53:26.000000Z",
                    "updated_at": "2023-02-20T12:53:26.000000Z",
                    "sub_questions": [
                        {
                            "id": 4,
                            "question": "Did you have to reduce your activity level?",
                            "type": "closed",
                            "created_by": null,
                            "medical_question_id": 12
                        }
                    ]
                }
            ],
            "message": ""
        }

        const wrapper = mount(Question);
        // let searchKeyword = wrapper.find("#searchID");
        // expect(searchKeyword.exists()).toBe(true);
        axios.get.mockResolvedValue()
        const res = await QuestionService.fetchQuestions();
        console.log('question success', res)
        // expect(res.data.status).equal(true)
        // expect(res.data.url).equal(mockResponseData.payload.url)
        // expect(res.data.statusCode).equal(mockResponseData.payload.statusCode)
        // expect(res.data.msg).equal(mockResponseData.payload.msg)
        // expect(res.data.data).equal(mockResponseData.payload.data)
        // expect(res.data.data).toHaveLength(2);



    });


   

    //  search_word: this.search_term
    //  it('Unsuccessfull retrieved invoices', async () => {
    //     const mockUrl = 'patients/invoices/all'
    //    const mockData = {};
    //     const mockResponseData = {
    //         status: false,
    //         error_code: 401
    //   };

    //    const wrapper = mount(Invoices);

     
    //      let searchKeyword = wrapper.find("#searchID");
    //      expect(searchKeyword.exists()).toBe(true);
    //      axios.post.mockResolvedValue({ data: mockResponseData })
    //      const res = await InvoiceService.fetchInvoices(mockUrl, mockData);
    //      console.log('failed displayed invoices', res)
    //      expect(res.data.status).equal(false)
    //      expect(res.data.status).equal(mockResponseData.status)
    //      expect(res.data.error_code).equal(mockResponseData.error_code)
       

    // });
});
